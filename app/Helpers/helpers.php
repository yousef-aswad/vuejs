<?php

if (!function_exists('user')) {
    function user()
    {
        return \Auth::user();
    }
}

if (!function_exists('isSuperUser')) {
    function isSuperUser($user)
    {
        $user = $user ?? user();
        if (!$user) {
            return null;
        }
        return $user->id == 1;
    }
}