<?php


namespace App\Classes;


use App\Models\Testimonial;

class CMS
{
    public function getTestimonials()
    {
        $testimonials = Testimonial::query()->get();
        return $testimonials;
    }

}