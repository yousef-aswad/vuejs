<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::query()->get();
        return response()->json(['testimonials' => $testimonials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'author' => 'required',
            'description' => 'required',
        ]);
        try {
            $data = $request->only(['author', 'description']);
            $testimonials = Testimonial::create($data);
            $message = "تم تخزين المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Testimonial $testimonials
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonials)
    {
        //
    }

    /**
     * @param Testimonial $testimonial
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Testimonial $testimonial)
    {
        return response()->json(['testimonial' => $testimonial]);
    }

    /**
     * @param Request $request
     * @param Testimonial $testimonial
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        $request->validate([
            'author' => 'required',
            'description' => 'required'
        ]);
        try {
            $data = $request->only(['author', 'description']);
            $testimonial->update($data);
            $message = "تم تعديل المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * @param Testimonial $testimonial
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Testimonial $testimonial)
    {
        try {
            $testimonial->delete();
            $message = "تم حذف المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }
}
