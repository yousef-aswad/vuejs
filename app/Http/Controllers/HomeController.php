<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    public function dashboard()
    {
        return view('dashboard.index');
    }

    public function home()
    {
        $posts = Post::query()->get();
        $categories = Category::query()->get();
        return view('home')->with((['posts' => $posts, 'categories' => $categories]));
    }

    public function mainHome()
    {
        return view('main_home');
    }
}
