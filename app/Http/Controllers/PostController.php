<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::query()->get();
        return response()->json(['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'categories' => 'required'
        ]);
        try {
            $data = $request->only(['title', 'body']);
            $categories = $request->get('categories');
            $myArray = explode(',', $categories);
            $post = Post::create($data);
            if ($request->hasFile('image')) {
                $post->addMedia($request->file('image'))->toMediaCollection($post->mediaCollectionName);
            }
            $post->categories()->sync($myArray);
            $message = "تم تخزين المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return response()->json(['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        try {
            $data = $request->only(['title', 'body']);
            $post->update($data);
            $message = "تم تعديل المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try {
            $post->delete();
            $message = "تم حذف المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }
}
