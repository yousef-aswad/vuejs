<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::query()->get();
        return response()->json(['categories' => $category]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        try {
            $data = $request->only(['name']);
            $category = Category::create($data);
            $message = "تم تخزين المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return response()->json(['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
        ]);
        try {
            $data = $request->only(['name']);
            $category->update($data);
            $message = "تم تخزين المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            $category->delete();
            $message = "تم حذف المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }
}
