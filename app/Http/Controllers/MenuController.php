<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::query()->get();
        return response()->json(['menus' => $menus]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required'
        ]);
        try {
            $data = $request->only(['title', 'url']);
            $menu = Menu::create($data);
            $message = "تم تخزين المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return response()->json(['menu' => $menu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required'
        ]);
        try {
            $data = $request->only(['title', 'url']);
            $menu->update($data);
            $message = "تم تعديل المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        try {
            $menu->delete();
            $message = "تم حذف المعلومات بنجاح !!";
        } catch (\Exception $exception) {
            $message = $exception->getMessage();
            logger($exception);
            $status = 400;
        }
        return response()->json(['message' => $message], $status ?? 200);
    }
}
