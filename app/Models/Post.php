<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Post extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $guarded = ['id'];

    public $mediaCollectionName = 'post-image';

    protected $table = "posts";

    public function getImageAttribute()
    {
        $media = $this->getFirstMedia($this->mediaCollectionName);
        if ($media) {
            return $media->getFullUrl();
        } else {
            return '';
        }
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
