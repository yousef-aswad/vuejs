<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'admin'], function () {
    Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::resource('posts', 'postController');
    Route::resource('menu', 'MenuController');
    Route::resource('category', 'CategoryController');
    Route::resource('testimonial', 'TestimonialController');
});