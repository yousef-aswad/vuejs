<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('admin/dashboard', 'HomeController@index')->middleware('auth');

Route::get('/home', 'HomeController@home');

Route::get('/main-home', 'HomeController@mainHome');

Route::get('/{any}', function () {
    return view('dashboard');
})->where('any', '.*')->middleware('auth');

