@extends('layouts.app')

@section('content')
    <div class="card" style="width: 220px;position: absolute;
    height: 100%;">
        <div class="card-header">sidebar</div>
        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <router-link to="/dashboard-posts">Posts</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/addPost">Add Posts</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/dashboard-menus">Menus</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/addMenu">Add Menus</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/dashboard-category">Categories</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/addCategory">Add Category</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/testimonials">Testimonials</router-link>
                </li>
                <li class="list-group-item">
                    <router-link to="/addTestimonials">Add Testimonials</router-link>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="p-5">
                    <router-view></router-view>
                </div>
            </div>
        </div>
    </div>
@endsection
