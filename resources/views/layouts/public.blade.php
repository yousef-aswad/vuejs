<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{asset('css/template.css')}}" type="text/css" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="header">
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="textured">
                            <div class="dropdown text-right">
                                @auth
                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown">
                                        {{ \Illuminate\Support\Facades\Auth::user()->name }}
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Profile</a>
                                        <a class="dropdown-item" href="{{url('admin/dashboard')}}">Dashboard</a>
                                        <a class="dropdown-item" href="{{route('logout')}}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                @else
                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown">
                                        My Account
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{route('login')}}">Login</a>
                                        <a class="dropdown-item" href="{{ route('register') }}">Register</a>
                                    </div>
                                @endauth
                            </div>
                            <h1 class="d-inline-block">Home Page</h1>
                            <span>blue</span>
                            <span>Simple. Contemporary. Website Template.</span>
                        </div>
                        @include('menu.menus')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
</div>
<script src="{{asset('js/app.js')}}" type="text/javascript" rel="script"></script>
</body>
</html>