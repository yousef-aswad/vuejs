@extends('layouts.public')

@section('content')
    <div class="content">
        <div class="container">
            <div class="row background-white">
                <div class="col-md-8">
                    @forelse($posts as $post)
                        <div class="posts">
                            <div class="single-post">
                                <h2>{{$post->title}}</h2>
                                <p class="paragrah">
                                    {!! $post->body !!}
                                </p>
                                @if($post->getMedia($post->mediaCollectionName))
                                    <div class="img-fluid custom-img">
                                        <img class="img-fluid" src="{{$post->image}}">
                                    </div>
                                @endif
                                @if($post->categories)
                                    <div class="categories">
                                        <ul class="category">
                                            @foreach($post->categories as $category)
                                                <li>{{$category->name}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @empty
                        <h2>There is no Posts</h2>
                    @endforelse
                </div>
                <div class="col-md-4">
                    <div class="categories text-center">
                        <h2>Categories :</h2>
                        <ul class="list-group text-left">
                            @foreach($categories as $category)
                                <li class="list-group-item">{{$category->name}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="testimonials">
        <div class="container">
            <div class="row background-white">
                <div class="col-md-8">
                    @include('testimonials.testimonials')
                </div>
            </div>
        </div>
    </div>
@endsection
