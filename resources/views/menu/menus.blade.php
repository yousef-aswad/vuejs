@php $menus = \App\Models\Menu::query()->get() @endphp
@if(!$menus->isEmpty())
    <div class="menu">
        <ul>
            @foreach($menus as $menu)
                <li>
                    <a href="{{url($menu->url)}}">{{$menu->title}}</a>
                </li>
            @endforeach
        </ul>
    </div>
@endif