<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS</title>
    <link href="{{mix('/css/app.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('/font-awesome-4.7.0/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<header>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 col-sm-12 col-lg-8 col-xl-8">
                    <div class="title-header">
                        <p>Lessons From just $20 Pre Hour or 5 Lessons for $120 or 10 Hours For $180</p>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-sm-12 col-lg-4 col-xl-4">
                    <div class="menu-contact">
                        <ul class="m-0 p-0">
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-google"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-logo-address">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-sm-12 col-lg-4 col-xl-4 d-flex align-items-lg-center">
                    <div class="img-logo d-flex align-items-lg-center">
                        <img src="{{asset('img/img-logo.png')}}" class="img-fluid">
                        <p class="m-0">DRIVING SCHOOL</p>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-sm-12 col-lg-8 col-xl-8">
                    <div class="address">
                        <ul class="m-0 p-0 d-flex justify-content-center align-items-lg-center">
                            <li>
                                <div class="location d-flex">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p class="m-0">MATVEY STREET, RUSSIA</p>
                                </div>
                            </li>
                            <li>
                                <div class="location d-flex">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <p class="m-0">+ 90 888 777 5544</p>
                                </div>
                            </li>
                            <li>
                                <button class="btn">CALL ME</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-menu">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="menu">
                        <ul class="m-0 p-0">
                            <li><a href="#">HOME</a></li>
                            <li><a href="#">ABOUT</a></li>
                            <li><a href="#">FEATURES</a></li>
                            <li><a href="#">PRICE</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">COURSES</a></li>
                            <li><a href="#">REVIEWS</a></li>
                            <li><a href="#">PHOTO</a></li>
                            <li><a href="#">BLOG</a></li>
                            <li><a href="#">CONTACTS</a></li>
                            <li><a href="#">TEACHERS</a></li>
                        </ul>
                    </div>
                    <div class="menu-responsive">
                        <div class="menus-res">
                            <ul class="m-0 p-0">
                                <li><a href="#">HOME</a></li>
                                <li><a href="#">ABOUT</a></li>
                                <li><a href="#">FEATURES</a></li>
                                <li><a href="#">PRICE</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">COURSES</a></li>
                                <li><a href="#">REVIEWS</a></li>
                                <li><a href="#">PHOTO</a></li>
                                <li><a href="#">BLOG</a></li>
                                <li><a href="#">CONTACTS</a></li>
                                <li><a href="#">TEACHERS</a></li>
                            </ul>
                        </div>
                        <a href="#" class="menu-res">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="form-driving">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 col-sm-12 col-lg-7 col-xl-7 d-flex justify-content-center align-items-lg-end">
                <div class="form-paragraph">
                    <h1>BEST SAFETY MEASURES</h1>
                    <p>Lorem ipsum Sunt cupidatat reprehenderit cillum reprehenderit incididunt
                        consequat aute et proident tempor occaecat.</p>
                </div>
            </div>
            <div class="col-12 col-md-12 col-sm-12 col-lg-5 col-xl-5">
                <div class="form">
                    <form>
                        <div class="form-title">
                            <h1>FIND</h1>
                            <span>DRIVING COURSES</span>
                            <p>Lorem ipsum Sunt cupidatat reprehenderit cillum reprehenderit incididunt...</p>
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup"
                                   placeholder="Enter your phone">
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-mail-reply-all"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup"
                                   placeholder="Enter your phone">
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup"
                                   placeholder="Enter your phone">
                        </div>
                        <div class="d-flex">
                            <div class="input-group mb-2 input-spilt">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-car"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                       placeholder="Enter your phone">
                            </div>
                            <div class="input-group mb-2 input-spilt">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-dribbble"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                       placeholder="Enter your phone">
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="input-group mb-2 input-spilt">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-database"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                       placeholder="Enter your phone">
                            </div>
                            <div class="input-group mb-2 input-spilt">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-times"></i>
                                    </div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                       placeholder="Enter your phone">
                            </div>
                        </div>
                        <div class="input-group">
                            <button class="btn">CALL ME</button>
                            <div class="footer-form">
                                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                <p>Lorem ipsum Sunt cupidatat reprehenderit cillum reprehenderit</p>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-form-driving">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="menu-car">
                        <ul class="m-0 p-0">
                            <li><a href="#">
                                    <i class="fa fa-car"></i>
                                    <h1>CAR TYPE B</h1>
                                    <span>Car Driving Learn</span>
                                </a></li>
                            <li><a href="#">
                                    <i class="fa fa-truck"></i>
                                    <h1>CAR TYPE C</h1>
                                    <span>Truck Driving Learn</span>
                                </a></li>
                            <li><a href="#">
                                    <i class="fa fa-bus"></i>
                                    <h1>CAR TYPE D</h1>
                                    <span>Bus Driving Learn</span>
                                </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="best-features">
    <div class="container">
        <div class="col-12 col-md-12">
            <section class="title">
                <h1>THE BEST</h1>
                <span>FEATURES</span>
            </section>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="experience d-flex align-items-lg-center section-public">
                    <div class="img-exp">
                        <img src="{{asset('img/img-icon.png')}}">
                    </div>
                    <div class="d-flex flex-column padding-35">
                        <h1>EXPERIENCE INSTRUCTORS</h1>
                        <p>Lorem ipsum Sunt cupidatat
                            reprehenderit cillum reprehenderit incididunt consequat</p>
                        <a href="#">See instructors <i class="fa fa fa-chevron-right"></i> </a>
                    </div>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="experience d-flex align-items-lg-center section-public">
                    <div class="img-exp">
                        <img src="{{asset('img/img-icon-two.png')}}">
                    </div>
                    <div class="d-flex flex-column padding-35">
                        <h1>VIDEO CLASSES</h1>
                        <p>Lorem ipsum Sunt cupidatat
                            reprehenderit cillum reprehenderit incididunt consequat</p>
                        <a href="#">See instructors <i class="fa fa fa-chevron-right"></i> </a>
                    </div>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="experience d-flex align-items-lg-center section-public">
                    <div class="img-exp">
                        <img src="{{asset('img/img-icon-three.png')}}">
                    </div>
                    <div class="d-flex flex-column padding-35">
                        <h1>EXPERIENCE INSTRUCTORS</h1>
                        <p>Lorem ipsum Sunt cupidatat
                            reprehenderit cillum reprehenderit incididunt consequat</p>
                        <a href="#">See instructors <i class="fa fa fa-chevron-right"></i> </a>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="about-as">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-sm-6 col-lg-7 col-xl-7">
                <section class="about">
                    <h1>ABOUT US</h1>
                    <p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque
                        laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto
                        beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit.

                        Aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi
                        nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor.

                        Sit amet, consectetur, adipisci velit, sed quia non numquam eius modi
                        tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. </p>
                    <button class="btn">SEND REQUEST</button>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-5 col-xl-5">
                <div class="p-lg-4 m-lg-5 m-lg-4"></div>
                <div class="order-request">
                    <section class="order">
                        <div class="save">
                            <h1>SAVE $59</h1>
                            <span>ORDER NOW !</span>
                        </div>
                    </section>
                    <section class="only-november">
                        <div class="title">
                            <p>Only in November! Book learning and receive
                                a discount of $ 59</p>
                            <div class="round">
                                <div class="" id="indicatorContainer"></div>
                                <div class="" id="indicatorContainer2"></div>
                                <div class="" id="indicatorContainer3"></div>
                                <div class="" id="indicatorContainer4"></div>
                            </div>
                            <button class="btn">SEND REQUEST</button>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="service">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <section class="best-service background-light">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service.png')}}">
                    </div>
                    <h1 class="text-center">BEST SERVICE</h1>
                    <p class="text-center">Lorem ipsum Sunt cupidatat
                        reprehenderit cillum reprehenderit incididunt consequat</p>
                </section>
                <section class="best-service">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service-person.png')}}">
                    </div>
                    <h1 class="text-center">WE TRAIN ALL AGES</h1>
                    <p class="text-center">we take into account the age
                        of individual moments.</p>
                </section>
                <section class="best-service background-light">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service.png')}}">
                    </div>
                    <h1 class="text-center">BEST SERVICE</h1>
                    <p class="text-center">Lorem ipsum Sunt cupidatat
                        reprehenderit cillum reprehenderit incididunt consequat</p>
                </section>
                <section class="best-service">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service-circle.png')}}">
                    </div>
                    <h1 class="text-center">Individual approach</h1>
                    <p class="text-center">
                        to everyone we have our own approach.
                    </p>
                </section>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <section class="best-service">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service-teacher.png')}}">
                    </div>
                    <h1 class="text-center">Calm instructor</h1>
                    <p class="text-center">practical training for you will be happy.</p>
                </section>
                <section class="best-service background-light">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service-public.png')}}">
                    </div>
                    <h1 class="text-center">duplicate pedals</h1>
                    <p class="text-center">Cars equipped with duplicate pedals - avtoinstruktor always insures you.</p>
                </section>
                <section class="best-service">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service-doc.png')}}">
                    </div>
                    <h1 class="text-center">ALL DOCUMENTS</h1>
                    <p class="text-center">
                        The full package of documents at the end - you get all the documents at the end of
                    </p>
                </section>
                <section class="best-service background-light">
                    <div class="img-service text-center">
                        <img src="{{asset('img/img-service-home.png')}}">
                    </div>
                    <h1 class="text-center">BEST CLASSROOM</h1>
                    <p class="text-center">Equipped classrooms - all done according to the rules and laws</p>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="counter">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                <section class="counter">
                    <h1>10 000+</h1>
                    <p>Graduates received the right</p>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                <section class="counter">
                    <h1>7</h1>
                    <p>Years in the market</p>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                <section class="counter">
                    <h1>578</h1>
                    <p>Training hours</p>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                <section class="counter">
                    <h1>32</h1>
                    <p>Number of teachers</p>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="price">
    <div class="container">
        <div class="row">
            <div class="col-8 col-md-8">
                <section class="title-price">
                    <h1>PRICE TABLE</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliqua</p>
                </section>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="basic-plan text-center">
                    <span>BESTSELLERS</span>
                    <h1>$ 2000</h1>
                    <span>Basic Plan</span>
                    <div class="features-plan">
                        <ul>
                            <li>
                                <div class="content">
                                    <i class="fa fa-check"></i>
                                    <p>Full course theory</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-check"></i>
                                    <p>Full driving course</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-check"></i>
                                    <p>Spending gasoline</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-close"></i>
                                    <p>Training in first aid</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-close"></i>
                                    <p>Practical Sessions</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-close"></i>
                                    <p>SMS reminders</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-close"></i>
                                    <p>Psychological support</p>
                                </div>
                            </li>
                            <li>
                                <button class="btn">SEND REQUEST</button>
                            </li>
                            <li class="rating">
                                <ul class="">
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="basic-plan text-center">
                    <span>BESTSELLERS</span>
                    <h1>$ 2000</h1>
                    <span>Basic Plan</span>
                    <div class="features-plan">
                        <ul>
                            <li>
                                <div class="content">
                                    <i class="fa fa-check"></i>
                                    <p>Full course theory</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-check"></i>
                                    <p>Full driving course</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-check"></i>
                                    <p>Spending gasoline</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-close"></i>
                                    <p>Training in first aid</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-close"></i>
                                    <p>Practical Sessions</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-close"></i>
                                    <p>SMS reminders</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-close"></i>
                                    <p>Psychological support</p>
                                </div>
                            </li>
                            <li>
                                <button class="btn">SEND REQUEST</button>
                            </li>
                            <li class="rating">
                                <ul class="">
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="basic-plan text-center">
                    <span>BESTSELLERS</span>
                    <h1>$ 2000</h1>
                    <span>Basic Plan</span>
                    <div class="features-plan">
                        <ul>
                            <li>
                                <div class="content">
                                    <i class="fa fa-check"></i>
                                    <p>Full course theory</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-check"></i>
                                    <p>Full driving course</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-check"></i>
                                    <p>Spending gasoline</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-close"></i>
                                    <p>Training in first aid</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-close"></i>
                                    <p>Practical Sessions</p>
                                </div>
                            </li>
                            <li>
                                <div class="content no-background">
                                    <i class="fa fa-close"></i>
                                    <p>SMS reminders</p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <i class="fa fa-close"></i>
                                    <p>Psychological support</p>
                                </div>
                            </li>
                            <li>
                                <button class="btn">SEND REQUEST</button>
                            </li>
                            <li class="rating">
                                <ul class="">
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                    <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="m-lg-45"></div>
    <div class="container">
        <div class="row">
            <div class="col-10 col-md-10">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <h1 class="title-slider text-center">Reviews</h1>
                    <section class="slider">
                        <div class="carousel slide" data-ride="carousel">
                            <ul class="carousel-indicators slider-one">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                            </ul>
                            <div class="carousel-inner">
                                <div class="carousel-item slider-one active">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt
                                        ut labore et dolore tempor incididun
                                        dolore tempor magna aliqua</p>
                                    <div class="author">
                                        <h1>Andry Lincoln</h1>
                                        <ul class="text-center">
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt
                                        ut labore et dolore tempor incididun
                                        dolore tempor magna aliqua</p>
                                    <div class="author">
                                        <h1>Andry Lincoln</h1>
                                        <ul class="text-center">
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt
                                        ut labore et dolore tempor incididun
                                        dolore tempor magna aliqua</p>
                                    <div class="author">
                                        <h1>Andry Lincoln</h1>
                                        <ul class="text-center">
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                            <li class="d-inline-block"><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <i class="fa fa-angle-double-left"
                                   style="color: #e8edf0;font-size: 50px;font-weight: bold;"></i>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                                <i class="fa fa-angle-double-right"
                                   style="color: #e8edf0;font-size: 50px;font-weight: bold;"></i>
                            </a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="courses">
    <div class="container">
        <div class="row">
            <div class="col-9 col-md-9">
                <section class="title-courses">
                    <h1>COURSES</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliqua</p>
                </section>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="block-course">
                    <div class="title">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h1>HOW TO PROTECT YOURSELF AND THE CAR</h1>
                    </div>
                    <div class="content">
                        <div class="part-one d-flex justify-content-between">
                            <p>Automatic</p>
                            <span>$300</span>
                        </div>
                        <div class="part-two d-flex justify-content-between">
                            <p>Mechanics</p>
                            <span>$400</span>
                        </div>
                    </div>
                    <div class="hover position-absolute">
                        <h1>TESTING ROUTES</h1>
                        <div class="days d-flex">
                            <i class="fa fa-clock-o"></i>
                            <p>30 DAYS</p>
                        </div>
                        <button class="btn">MORE INFO</button>
                        <button class="btn">BUY</button>
                    </div>
                </section>
                <section class="block-course">
                    <div class="title">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h1>HOW TO PROTECT YOURSELF AND THE CAR</h1>
                    </div>
                    <div class="content">
                        <div class="part-one d-flex justify-content-between">
                            <p>Automatic</p>
                            <span>$300</span>
                        </div>
                        <div class="part-two d-flex justify-content-between">
                            <p>Mechanics</p>
                            <span>$400</span>
                        </div>
                    </div>
                    <div class="hover position-absolute">
                        <h1>TESTING ROUTES</h1>
                        <div class="days d-flex">
                            <i class="fa fa-clock-o"></i>
                            <p>30 DAYS</p>
                        </div>
                        <button class="btn">MORE INFO</button>
                        <button class="btn">BUY</button>
                    </div>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="block-course">
                    <div class="title">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h1>HOW TO PROTECT YOURSELF AND THE CAR</h1>
                    </div>
                    <div class="content">
                        <div class="part-one d-flex justify-content-between">
                            <p>Automatic</p>
                            <span>$300</span>
                        </div>
                        <div class="part-two d-flex justify-content-between">
                            <p>Mechanics</p>
                            <span>$400</span>
                        </div>
                    </div>
                    <div class="hover position-absolute">
                        <h1>TESTING ROUTES</h1>
                        <div class="days d-flex">
                            <i class="fa fa-clock-o"></i>
                            <p>30 DAYS</p>
                        </div>
                        <button class="btn">MORE INFO</button>
                        <button class="btn">BUY</button>
                    </div>
                </section>
                <section class="block-course">
                    <div class="content-consul text-center">
                        <div class="icon d-flex justify-content-center">
                            <i class="fa fa-car"></i>
                            <p class="text-center d-inline-block">CONSULTATION</p>
                        </div>
                    </div>
                    <div class="hover position-absolute">
                        <h1>TESTING ROUTES</h1>
                        <div class="days d-flex">
                            <i class="fa fa-clock-o"></i>
                            <p>30 DAYS</p>
                        </div>
                        <button class="btn">MORE INFO</button>
                        <button class="btn">BUY</button>
                    </div>
                </section>
            </div>
            <div class="col-12 col-md-6 col-sm-6 col-lg-4 col-xl-4">
                <section class="block-course">
                    <div class="title">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h1>HOW TO PROTECT YOURSELF AND THE CAR</h1>
                    </div>
                    <div class="content">
                        <div class="part-one d-flex justify-content-between">
                            <p>Automatic</p>
                            <span>$300</span>
                        </div>
                        <div class="part-two d-flex justify-content-between">
                            <p>Mechanics</p>
                            <span>$400</span>
                        </div>
                    </div>
                    <div class="hover position-absolute">
                        <h1>TESTING ROUTES</h1>
                        <div class="days d-flex">
                            <i class="fa fa-clock-o"></i>
                            <p>30 DAYS</p>
                        </div>
                        <button class="btn">MORE INFO</button>
                        <button class="btn">BUY</button>
                    </div>
                </section>
                <section class="block-course">
                    <div class="content-consul">
                        <div class="icon d-flex justify-content-center">
                            <i class="fa fa-car"></i>
                            <p class="text-center d-inline-block">Practice skills of
                                traffic police</p>
                        </div>
                    </div>
                    <div class="hover position-absolute">
                        <h1>TESTING ROUTES</h1>
                        <div class="days d-flex">
                            <i class="fa fa-clock-o"></i>
                            <p>30 DAYS</p>
                        </div>
                        <button class="btn">MORE INFO</button>
                        <button class="btn">BUY</button>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="photo-gallery">
    <div class="container">
        <div class="row">
            <div class="col-9 col-md-9">
                <section class="photo-title">
                    <h1>PHOTO GALLERY</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliqua</p>
                </section>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav-photo-gallery p-0">
                    <li class="">
                        <a class="active" data-toggle="tab" href="#cars">Cars</a>
                    </li>
                    <li class="">
                        <a class="" data-toggle="tab" href="#drom">Auto drom</a>
                    </li>
                    <li class="">
                        <a class="" data-toggle="tab" href="#Classroom">Classroom</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane container active p-0" id="cars">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-6 col-sm-12 col-lg-6 col-xl-3">
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-sm-12 col-lg-6 col-xl-3">
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-sm-12 col-lg-6 col-xl-3">
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-sm-12 col-lg-6 col-xl-3">
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                    <div class="img">
                                        <img src="{{asset('img/img-blue.png')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="drom"></div>
                    <div class="tab-pane container fade" id="Classroom"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="play-video">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <section class="video">
                    <div class="icon-video">
                        <i class="fa fa-play" aria-hidden="true"></i>
                    </div>
                    <div class="content text-center">
                        <h1>SEE</h1>
                        <span>VIDEO PRESENTATION</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua</p>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="instructors">
    <div class="container">
        <div class="row">
            <div class="col-9 col-md-9">
                <section class="photo-title">
                    <h1>INSTRUCTORS</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliqua</p>
                </section>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav-photo-gallery p-0">
                    <li class="">
                        <a class="active" data-toggle="tab" href="#theory">Theory</a>
                    </li>
                    <li class="">
                        <a class="" data-toggle="tab" href="#practice">Practice</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane container active p-0" id="theory">
                        <div class="container">
                            <div class="">
                                <div id="demo1" class="carousel slide" data-ride="carousel">
                                    <section class="slider-instructors container">
                                        <div class="carousel slide row" data-ride="carousel">
                                            <div class="col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                                <div class="img-slider-two">
                                                    <img src="{{asset('img/img-slider-tab.png')}}">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
                                                <div class="p-lg-4"></div>
                                                <div class="p-5 p-3"></div>
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <div class="owner">
                                                            <h1>IVANOV ALEXEY</h1>
                                                            <p>Theory instructor</p>
                                                        </div>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                            do
                                                            eiusmod tempor
                                                            incididunt
                                                            ut labore et dolore tempor incididun
                                                            dolore tempor magna aliqua</p>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="owner">
                                                            <h1>IVANOV ALEXEY</h1>
                                                            <p>Theory instructor</p>
                                                        </div>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                            do
                                                            eiusmod tempor
                                                            incididunt
                                                            ut labore et dolore tempor incididun
                                                            dolore tempor magna aliqua</p>
                                                    </div>
                                                    <div class="carousel-item">
                                                        <div class="owner">
                                                            <h1>IVANOV ALEXEY</h1>
                                                            <p>Theory instructor</p>
                                                        </div>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                                            do
                                                            eiusmod tempor
                                                            incididunt
                                                            ut labore et dolore tempor incididun
                                                            dolore tempor magna aliqua</p>
                                                    </div>
                                                </div>
                                                <ul class="carousel-indicators slider-two">
                                                    <li data-target="#demo1" data-slide-to="0" class="active"></li>
                                                    <li data-target="#demo1" data-slide-to="1"></li>
                                                    <li data-target="#demo1" data-slide-to="2"></li>
                                                </ul>
                                                <a class="carousel-control-prev slider-two-prev" href="#demo1"
                                                   data-slide="prev">
                                                    <i class="fa fa-angle-double-left"
                                                       style="color: #e8edf0;font-size: 50px;font-weight: bold;"></i>
                                                </a>
                                                <a class="carousel-control-next slider-two-next" href="#demo1"
                                                   data-slide="next">
                                                    <i class="fa fa-angle-double-right"
                                                       style="color: #e8edf0;font-size: 50px;font-weight: bold;"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="practice"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="map">
    <map>
        <iframe src="https://maps.google.com/maps?q=10153&hl=en&z=14&amp;output=embed"
                width="100%" height="714" frameborder="0"
                style="border:0"
                allowfullscreen></iframe>
    </map>
    <section class="form-map">
        <form>
            <div class="contact">
                <ul>
                    <li><i class="fa fa-location-arrow"></i>
                        <p>MATVEY STREET , RUSSIA</p></li>
                    <li><i class="fa fa-phone"></i>
                        <p>+90 888 777 5544</p></li>
                </ul>
            </div>
            <div class="input-form">
                <div class="d-flex">
                    <div class="input-group input-spilt">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control" id="inlineFormInputGroup"
                               placeholder="Enter your name">
                    </div>
                    <div class="input-group  input-spilt">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-inbox"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control" id="inlineFormInputGroup"
                               placeholder="Enter your E-mail">
                    </div>
                </div>
                <div class="input-group mb-2">
                <textarea type="text" class="form-control" id="inlineFormInputGroup"
                          placeholder="Enter your Message">
                </textarea>
                </div>
                <button class="btn">SEND REQUEST</button>
            </div>
        </form>
    </section>
</div>
<div class="faq">
    <div class="container">
        <div class="row">
            <div class="col-7 col-md-7">
                <section class="faq">
                    <h1>FAQS</h1>
                    <div id="accordion">
                        <div class="">
                            <div class="" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn faq" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fa fa-minus"></i> How long is the road test appointment?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                 data-parent="#accordion">
                                <div class="content-faq">
                                    Yes you can. your parent or guardian must sign up the form MV-44. also, New York
                                    Motor Vehicles requires at least 50 hours of supervised training of 15 hours must be
                                    night driving in order to take your Road Test.
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn faq" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="true" aria-controls="collapseTwo">
                                        <i class="fa fa-minus"></i> How long is the road test appointment?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                                 data-parent="#accordion">
                                <div class="content-faq">
                                    Yes you can. your parent or guardian must sign up the form MV-44. also, New York
                                    Motor Vehicles requires at least 50 hours of supervised training of 15 hours must be
                                    night driving in order to take your Road Test.
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn faq" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="true" aria-controls="collapseThree">
                                        <i class="fa fa-minus"></i> How long is the road test appointment?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseThree" class="collapse show" aria-labelledby="headingThree"
                                 data-parent="#accordion">
                                <div class="content-faq">
                                    Yes you can. your parent or guardian must sign up the form MV-44. also, New York
                                    Motor Vehicles requires at least 50 hours of supervised training of 15 hours must be
                                    night driving in order to take your Road Test.
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="" id="headingFour">
                                <h5 class="mb-0">
                                    <button class="btn faq" data-toggle="collapse" data-target="#collapseFour"
                                            aria-expanded="true" aria-controls="collapseFour">
                                        <i class="fa fa-minus"></i> How long is the road test appointment?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFour" class="collapse show" aria-labelledby="headingFour"
                                 data-parent="#accordion">
                                <div class="content-faq">
                                    Yes you can. your parent or guardian must sign up the form MV-44. also, New York
                                    Motor Vehicles requires at least 50 hours of supervised training of 15 hours must be
                                    night driving in order to take your Road Test.
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="" id="headingFive">
                                <h5 class="mb-0">
                                    <button class="btn faq" data-toggle="collapse" data-target="#collapseFive"
                                            aria-expanded="true" aria-controls="collapseFive">
                                        <i class="fa fa-minus"></i> How long is the road test appointment?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseFive" class="collapse show" aria-labelledby="headingFive"
                                 data-parent="#accordion">
                                <div class="content-faq">
                                    Yes you can. your parent or guardian must sign up the form MV-44. also, New York
                                    Motor Vehicles requires at least 50 hours of supervised training of 15 hours must be
                                    night driving in order to take your Road Test.
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="home-menu">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="menu">
                        <ul class="m-0 p-0">
                            <li><a href="#">HOME</a></li>
                            <li><a href="#">ABOUT</a></li>
                            <li><a href="#">FEATURES</a></li>
                            <li><a href="#">PRICE</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">COURSES</a></li>
                            <li><a href="#">REVIEWS</a></li>
                            <li><a href="#">PHOTO</a></li>
                            <li><a href="#">BLOG</a></li>
                            <li><a href="#">CONTACTS</a></li>
                            <li><a href="#">TEACHERS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                    <section class="about-footer">
                        <h1>ABOUT</h1>
                        <p>have been serving NYC with its
                            8 branches and completed 5k+ students in last 10 years. We have a team of experienced and
                            certified trainers who will help you from the start to end of a driving lesson.</p>
                        <div class="icon">
                            <p>DRIVING SCHOOL</p>
                        </div>
                    </section>
                </div>
                <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                    <section class="blog-footer">
                        <h1>BLOG</h1>
                        <div class="blog-mount">
                            <h2>Mountains car travel</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</p>
                        </div>
                        <div class="blog-mount">
                            <h2>Mountains car travel</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</p>
                        </div>
                        <a href="#">SEE ALL POSTS <i class="fa fa-arrow-right"></i> </a>
                    </section>
                </div>
                <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                    <section class="blog-footer">
                        <h1>INSTAGRAM</h1>
                        <div class="insta-rectangle">
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                            <div class="rec"></div>
                        </div>
                        <a href="#">SEE ALL PHOTOS <i class="fa fa-arrow-right"></i> </a>
                    </section>
                </div>
                <div class="col-12 col-md-6 col-sm-6 col-lg-3 col-xl-3">
                    <section class="blog-footer">
                        <h1>OPENING HOURS</h1>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                        <div class="part-one d-flex justify-content-between">
                            <p class="opening position-relative">Monday</p>
                            <span class="opening">08:00 - 18:00</span>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="last-footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <p>Design by - MatveyAn</p>
                    <p>Development by - Alexander</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('js/app.js')}}" type="text/javascript" rel="script"></script>
<script type="text/javascript">
    $(document).on('click', '.nav-photo-gallery li a', function () {
        $(".nav-photo-gallery li a").removeClass("active");
        $(this).addClass("active");
    });
    $(".menu-res").click(function () {
        $(".menus-res").toggleClass("active");
    });
</script>
</body>
</html>