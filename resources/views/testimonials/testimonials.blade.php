@php $testimonials = \CMS::getTestimonials(); @endphp
@if(!$testimonials->isEmpty())
    <div class="testimonials-title">
        <h2>Testimonials:-</h2>
        @forelse($testimonials as $testimonial)
            <div class="posts">
                <div class="single-post">
                    <h2>{{$testimonial->author}}</h2>
                    <p class="paragrah">
                        {!! $testimonial->description !!}
                    </p>
                </div>
            </div>
        @empty
            <h2>There is no Testimonials</h2>
        @endforelse
    </div>
@endif