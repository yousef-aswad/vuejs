import AddMenu from "./components/AddMenu";

require('./bootstrap');
window.Vue = require('vue');
window.VueRouter = require('vue-router').default;
window.VueAxios = require('vue-axios').default;
window.Axios = require('axios').default;
Vue.use(VueRouter, VueAxios, Axios);

//post Component
Vue.component('dashboardPosts', require('./components/dashboardPosts.vue').default);
Vue.component('posts', require('./components/ListsPosts.vue').default);
Vue.component('addPosts', require('./components/AddPost.vue').default);
Vue.component('editPost', require('./components/EditPost.vue').default);

//Testimonials Component
Vue.component('dashboardTestimonials', require('./components/testimonials.vue').default);
Vue.component('addTestimonials', require('./components/AddTestimonials').default);
Vue.component('editTestimonial', require('./components/EditTestimonials').default);
//Menu Component
Vue.component('menus', require('./components/dashboardMenu.vue').default);
Vue.component('addMenu', require('./components/AddMenu.vue').default);
Vue.component('editMenu', require('./components/EditMenu.vue').default);


//category Component
Vue.component('categories', require('./components/dashboardCategory.vue').default);
Vue.component('addCategory', require('./components/AddCategory.vue').default);
Vue.component('editCategory', require('./components/EditCategory.vue').default);


Vue.component('home', require('./components/home.vue').default);

//post.vue
import dashboardPosts from "./components/dashboardPosts.vue";
import posts from "./components/ListsPosts.vue";
import addPosts from "./components/AddPost.vue";
import editPosts from "./components/EditPost.vue";

//testimonials.vue
import dashboardTestimonials from "./components/testimonials.vue";
import addTestimonials from "./components/AddTestimonials";
import editTestimonial from "./components/EditTestimonials";
//menu.vue
import menus from "./components/dashboardMenu.vue";
import addMenu from "./components/AddMenu.vue";
import editMenu from "./components/EditMenu.vue";
//categories
import categories from "./components/dashboardCategory.vue";
import addCategory from "./components/AddCategory.vue";
import editCategory from "./components/EditCategory.vue";


import home from "./components/home.vue";

const routes = [
    {
        name: 'listsPosts',
        path: '/posts',
        component: posts
    },
    {
        name: 'dashboardPosts',
        path: '/dashboard-posts',
        component: dashboardPosts,
    },
    {
        name: 'create',
        path: '/addPost',
        component: addPosts
    },
    {
        name: 'editPost',
        path: '/admin/posts/:id/edit',
        component: editPosts
    },
    {
        name: 'addMenu',
        path: '/addMenu',
        component: AddMenu,
    },
    {
        name: 'editMenu',
        path: '/admin/menu/:id/edit',
        component: editMenu,
    },
    {
        name: 'listMenu',
        path: '/dashboard-menus',
        component: menus,
    },
    {
        name: 'categories',
        path: '/dashboard-category',
        component: categories,
    },
    {
        name: 'addCategory',
        path: '/addCategory',
        component: addCategory,
    },
    {
        name: 'editCategory',
        path: '/admin/category/:id/edit',
        component: editCategory,
    },
    {
        name: 'home',
        path: '/',
        component: home
    },
    {
        name: 'testimonials',
        path: '/testimonials',
        component: dashboardTestimonials,
    },
    {
        name: 'addTestimonials',
        path: '/addTestimonials',
        component: addTestimonials,
    },
    {
        name: 'editTestimonial',
        path: '/admin/testimonial/:id/edit',
        component: editTestimonial,
    },
];
const router = new VueRouter({mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({router})).$mount('#app');